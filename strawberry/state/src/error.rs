use thiserror::Error as ThisError;

pub type StateResult<T> = Result<T, StateError>;

#[derive(ThisError, Debug)]
#[allow(dead_code)]
pub enum StateError {
  #[error("Missing {0} config")]
  MissingConfig(String),
  #[error(transparent)]
  Rbatis(#[from] rbatis::Error),
  #[error("Custom: {0}")]
  Custom(String),
}
