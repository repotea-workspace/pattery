use std::time::Duration;

use actix_web::web;
use rbatis::db::DBPoolOptions;
use rbatis::rbatis::Rbatis;

use strawberry_config::types::StrawberryConfig;

use crate::error::{StateError, StateResult};
use crate::global;

/// App state
pub type AppState = web::Data<StrawberryState>;

/// App state
#[derive(Clone)]
pub struct StrawberryState {}

impl StrawberryState {
  /// Create app state
  fn app_state(self) -> AppState {
    web::Data::new(self)
  }
}

impl StrawberryState {
  pub fn rbatis(&self) -> StateResult<&Rbatis> {
    global::RBATIS
      .get()
      .ok_or_else(|| StateError::Custom("The database is not initialized".to_string()))
  }
}

/// app state builder
#[derive(Clone, Debug)]
pub struct AppStateBuilder {
  config: StrawberryConfig,
}

impl AppStateBuilder {
  /// create new builder instance
  pub fn new(config: StrawberryConfig) -> Self {
    Self { config }
  }
}

impl AppStateBuilder {
  /// get app state
  pub async fn app_state(&self) -> StateResult<AppState> {
    let _rbatis = self.start_rbatis().await?;
    let strawberry_state = StrawberryState {};
    Ok(strawberry_state.app_state())
  }

  async fn start_rbatis(&self) -> StateResult<&Rbatis> {
    let dc = match &self.config.database {
      Some(v) => v,
      None => return Err(StateError::MissingConfig("database".to_string())),
    };
    let mut opt = DBPoolOptions::new();
    let pool_options = &dc.pool;
    opt.max_connections = pool_options.max_connections;
    opt.min_connections = pool_options.min_connections;
    opt.connect_timeout = Duration::from_secs(pool_options.connect_timeout);
    opt.idle_timeout = pool_options.idle_timeout.map(Duration::from_secs);
    opt.max_lifetime = pool_options.max_lifetime.map(Duration::from_secs);
    opt.test_before_acquire = pool_options.test_before_acquire;

    let drive_url = format!(
      "{}://{}:{}@{}:{}/{}",
      dc.driver, dc.user, dc.password, dc.host, dc.port, dc.dbname,
    );
    let rbatis = Rbatis::new();
    rbatis
      .link_opt(&drive_url, opt)
      .await
      .map_err(StateError::Rbatis)?;
    global::RBATIS
      .set(rbatis)
      .map_err(|_e| StateError::Custom("Failed to set global stat for rbatis".to_string()))?;

    tracing::info!(
      target: "strawberry-state",
      "The database pool created with host: [{}]",
      dc.host,
    );
    Ok(global::RBATIS.get().unwrap())
  }
}
