use once_cell::sync::OnceCell;
use rbatis::rbatis::Rbatis;

use crate::error::{StateError, StateResult};

/// rbatis global state
pub static RBATIS: OnceCell<Rbatis> = OnceCell::new();

pub fn rbatis() -> StateResult<&'static Rbatis> {
  RBATIS
    .get()
    .ok_or_else(|| StateError::Custom("The rbatis is not initialize".to_string()))
}
