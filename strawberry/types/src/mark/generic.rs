use serde::{Deserialize, Serialize};

/// Yes mark
#[derive(Clone, Debug, Deserialize, Serialize, strum::EnumString, strum::Display)]
#[serde(rename_all = "kebab-case")]
#[strum(serialize_all = "kebab-case")]
pub enum Yes {
  /// yes
  Yes,
  /// no
  No,
}

impl Yes {
  /// The number value for Yes mark
  pub fn number(&self) -> u16 {
    match self {
      Self::Yes => 1,
      Self::No => 0,
    }
  }
}

impl Default for Yes {
  fn default() -> Self {
    Self::No
  }
}
