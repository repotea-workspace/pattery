/// page query interface
pub trait IPageQuery {
  fn pn(&self) -> u64;
  fn ps(&self) -> u64;
}
