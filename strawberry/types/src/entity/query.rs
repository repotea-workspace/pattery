use serde::{Deserialize, Serialize};
use serde_aux::prelude::deserialize_number_from_string;

use crate::entity::traits::IPageQuery;

/// Query with id
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct QueryWithId {
  /// id
  pub id: String,
}

/// Query with ids
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct QueryWithIds {
  /// ids
  pub ids: Vec<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct QueryWithPage {
  /// page number
  #[serde(deserialize_with = "deserialize_number_from_string")]
  pub pn: u64,
  /// page size
  #[serde(deserialize_with = "deserialize_number_from_string")]
  pub ps: u64,
}

impl IPageQuery for QueryWithPage {
  fn pn(&self) -> u64 {
    self.pn
  }

  fn ps(&self) -> u64 {
    self.ps
  }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct QueryWithKeyword {
  pub kw: Option<String>,
}
