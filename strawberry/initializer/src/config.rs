use std::path::Path;

use config::Config;
use serde::de::DeserializeOwned;

use strawberry_common::constants;
use strawberry_config::types::StrawberryConfig;

use crate::error::{InitializeError, InitializeResult};

/// Init config
pub fn init_config() -> InitializeResult<StrawberryConfig> {
  let name = "application";
  let base_path = constants::strawberry_home();
  let config = load_from_file(name, &base_path)?;
  Ok(config)
}

fn load_from_file<T: DeserializeOwned>(
  name: impl AsRef<str>,
  base_path: &Path,
) -> InitializeResult<T> {
  let environment = config::Environment::default()
    .prefix("STRAWBERRY")
    .separator("_");
  let mut basic_config = Config::builder();
  if base_path.exists() {
    let path_text = base_path
      .to_str()
      .ok_or_else(|| InitializeError::Config("Failed to parse base_path".to_string()))?;
    let basic_file_name = format!("{}/{}", path_text, name.as_ref());
    basic_config = basic_config.add_source(config::File::with_name(&basic_file_name));
  }
  let basic_config = basic_config
    .add_source(environment)
    .build()
    .map_err(|e| InitializeError::Config(format!("Failed to load config: {e:?}")))?;
  let app_config = basic_config
    .try_deserialize::<T>()
    .map_err(|e| InitializeError::Config(format!("Failed to parse config: {e:?}")))?;
  Ok(app_config)
}
