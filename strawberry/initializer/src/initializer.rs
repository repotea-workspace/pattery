use strawberry_config::types::StrawberryConfig;

use crate::error::{InitializeError, InitializeResult};
use crate::{config, logger};

/// app initializer
pub struct AppInitializer;

impl AppInitializer {
  pub fn init(&self) -> InitializeResult<StrawberryConfig> {
    self.init_color_eyre()?;
    let config = self.init_config()?;
    self.init_logger(&config)?;
    Ok(config)
  }
}

impl AppInitializer {
  /// init config
  fn init_config(&self) -> InitializeResult<StrawberryConfig> {
    config::init_config()
  }

  /// init color eyre
  fn init_color_eyre(&self) -> InitializeResult<()> {
    color_eyre::install().map_err(|e| InitializeError::ColorEyre(format!("{e:?}")))
  }

  /// init logger
  fn init_logger(&self, config: &StrawberryConfig) -> InitializeResult<()> {
    logger::init_log(config)
  }
}
