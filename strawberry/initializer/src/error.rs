use thiserror::Error as ThisError;

pub type InitializeResult<T> = Result<T, InitializeError>;

#[derive(ThisError, Debug)]
#[allow(dead_code)]
pub enum InitializeError {
  #[error("Initialize logger: {0}")]
  Logger(String),
  #[error("Initialize color-eyre: {0}")]
  ColorEyre(String),
  #[error("Initialize config: {0}")]
  Config(String),
}
