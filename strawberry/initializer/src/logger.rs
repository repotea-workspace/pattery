use std::str::FromStr;

use tracing::Level;
use tracing_subscriber::fmt::time::ChronoUtc;
use tracing_subscriber::EnvFilter;

use strawberry_config::types::StrawberryConfig;

use crate::error::InitializeResult;

pub fn init_log(config: &StrawberryConfig) -> InitializeResult<()> {
  let def_log_filter = config.logger.filter.join(",");

  let use_json_adapter = &config.logger.adapter == "json";
  let max_log_level = Level::from_str(&config.logger.max_level).unwrap_or(Level::TRACE);

  if use_json_adapter {
    tracing_subscriber::FmtSubscriber::builder()
      .with_max_level(max_log_level)
      .with_env_filter(
        EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::from(def_log_filter)),
      )
      .json()
      .init();
    // tracing::subscriber::set_global_default(subscriber)
    //     .expect("setting default subscriber failed");
    return Ok(());
  }

  tracing_subscriber::FmtSubscriber::builder()
    .with_max_level(max_log_level)
    .with_env_filter(
      EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::from(def_log_filter)),
    )
    // https://docs.rs/chrono/0.4.19/chrono/format/strftime/index.html
    .with_timer(ChronoUtc::with_format("%F %T".to_string()))
    .init();
  // tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
  Ok(())
}
