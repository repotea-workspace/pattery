use serde::{Deserialize, Serialize};

/// App config
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct StrawberryConfig {
  /// app server config
  #[serde(default)]
  pub server: ServerConfig,
  /// logger config
  #[serde(default)]
  pub logger: LoggerConfig,
  /// database config
  #[serde(default)]
  pub database: Option<DatabaseConfig>,
  /// sentry config
  #[serde(default)]
  pub sentry: Option<SentryConfig>,
}

/// App server config
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ServerConfig {
  /// bind host
  #[serde(default = "value_default::default_server_host")]
  pub host: String,
  /// bind port
  #[serde(default = "value_default::default_server_port")]
  pub port: u16,
}

/// Logger config
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LoggerConfig {
  /// logger filter
  #[serde(default = "value_default::default_logger_filter")]
  pub filter: Vec<String>,
  /// logger adapter
  #[serde(default = "value_default::default_logger_adapter")]
  pub adapter: String,
  #[serde(default = "value_default::default_logger_max_level")]
  pub max_level: String,
}

/// Sentry config
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SentryConfig {
  /// sentry dsn
  #[serde(default)]
  pub dsn: String,
}

/// Database config
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DatabaseConfig {
  /// database driver
  pub driver: String,
  /// user
  pub user: String,
  /// password
  pub password: String,
  /// database name
  pub dbname: String,
  /// host
  pub host: String,
  /// port
  pub port: u16,
  /// Db pool options
  #[serde(default)]
  pub pool: DatabasePoolOptions,
}

/// database pool options
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DatabasePoolOptions {
  /// max connections
  #[serde(default = "value_default::default_pool_max_connections")]
  pub max_connections: u32,
  /// min connections
  #[serde(default = "value_default::default_pool_min_connections")]
  pub min_connections: u32,
  /// connect timeout
  #[serde(default = "value_default::default_pool_connect_timeout")]
  pub connect_timeout: u64,
  /// max lifetime
  pub max_lifetime: Option<u64>,
  /// idle timeout
  pub idle_timeout: Option<u64>,
  /// test before acquire
  #[serde(default)]
  pub test_before_acquire: bool,
}

impl Default for DatabasePoolOptions {
  fn default() -> Self {
    Self {
      // pool a maximum of 10 connections to the same database
      max_connections: value_default::default_pool_max_connections(),
      // don't open connections until necessary
      min_connections: value_default::default_pool_min_connections(),
      // try to connect for 10 seconds before erroring
      connect_timeout: value_default::default_pool_connect_timeout(),
      // reap connections that have been alive > 30 minutes
      // prevents unbounded live-leaking of memory due to naive prepared statement caching
      // see src/cache.rs for context
      max_lifetime: Some(1800),
      // don't reap connections based on idle time
      idle_timeout: None,
      // If true, test the health of a connection on acquire
      test_before_acquire: true,
    }
  }
}

/// Logger adapter
#[derive(Clone, Debug, Deserialize, Serialize, strum::EnumString, strum::Display)]
#[serde(rename_all = "kebab-case")]
#[strum(serialize_all = "kebab-case")]
pub enum LoggerAdapter {
  Json,
  Raw,
}

impl Default for ServerConfig {
  fn default() -> Self {
    Self {
      host: value_default::default_server_host(),
      port: value_default::default_server_port(),
    }
  }
}

impl Default for LoggerConfig {
  fn default() -> Self {
    Self {
      filter: value_default::default_logger_filter(),
      adapter: value_default::default_logger_adapter(),
      max_level: value_default::default_logger_max_level(),
    }
  }
}

mod value_default {
  pub fn default_server_host() -> String {
    "127.0.0.1".to_string()
  }

  pub fn default_server_port() -> u16 {
    5000
  }

  pub fn default_logger_filter() -> Vec<String> {
    vec![
      "info",
      "hyper=error",
      "isahc=error",
      "strawberry=trace",
      "strawberry-state=trace",
    ]
    .iter()
    .map(|item| item.to_string())
    .collect()
  }

  pub fn default_logger_adapter() -> String {
    "raw".to_string()
  }

  pub fn default_logger_max_level() -> String {
    "info".to_string()
  }

  pub fn default_pool_max_connections() -> u32 {
    15
  }

  pub fn default_pool_min_connections() -> u32 {
    1
  }

  pub fn default_pool_connect_timeout() -> u64 {
    60
  }
}
