use std::path::{Path, PathBuf};

use crate::envh;

/// Get strawberry home
pub fn strawberry_home() -> PathBuf {
  let path_env = envh::strawberry_home();
  let is_from_env = path_env.is_some();
  let basic_path = path_env
    .map(|v| Path::new(&v).join(""))
    .or_else(dirs::home_dir)
    .or_else(|| {
      std::env::current_exe()
        .map(|v| v.parent().map(|p| p.to_path_buf()))
        .ok()
        .flatten()
    })
    .unwrap_or_else(std::env::temp_dir);
  let mut base_path = basic_path;
  if !is_from_env {
    base_path = base_path.join(".strawberry");
  }
  base_path
}
