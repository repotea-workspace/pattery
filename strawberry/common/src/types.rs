/// App running mode
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum RunningMode {
  Production,
  Staging,
  Develop,
}

impl RunningMode {
  /// get instance of text
  pub fn of(text: impl AsRef<str>) -> Self {
    let uppercase = text.as_ref().to_uppercase();
    match &uppercase[..] {
      "PRODUCTION" => Self::Production,
      "STAGING" => Self::Staging,
      "DEVELOP" => Self::Develop,
      _ => Self::Develop,
    }
  }
}
