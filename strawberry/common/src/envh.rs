use crate::types::RunningMode;

/// Get strawberry home from env
pub fn strawberry_home() -> Option<String> {
  std::env::var("STRAWBERRY_HOME").ok()
}

/// App running mode
pub fn running_mode() -> RunningMode {
  std::env::var("STRATEBERRY_RUNNING_MODE")
    .map(RunningMode::of)
    .unwrap_or(RunningMode::Develop)
}
