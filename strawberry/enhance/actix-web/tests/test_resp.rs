use strawberry_actix_web::types::Resp;

#[test]
fn test_to_json() {
  let resp = Resp::ok_with_data("");
  let json = resp.to_json().unwrap();
  assert_eq!(r#"{"err":false,"data":""}"#, json);
  let resp = Resp::ok();
  let json = resp.to_json().unwrap();
  assert_eq!(r#"{"err":false,"message":"success"}"#, json);
}
