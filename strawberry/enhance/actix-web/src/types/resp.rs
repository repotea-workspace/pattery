use serde::{Deserialize, Serialize};

/// response type
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Resp<T> {
  err: bool,
  #[serde(skip_serializing_if = "Option::is_none")]
  message: Option<String>,
  #[serde(skip_serializing_if = "Option::is_none")]
  data: Option<T>,
}

impl<T> Resp<T> {
  pub fn err_full(data: T, message: impl AsRef<str>) -> Self {
    Self {
      err: true,
      message: Some(message.as_ref().to_string()),
      data: Some(data),
    }
  }

  pub fn err_with_data(data: T) -> Self {
    Self {
      err: true,
      message: None,
      data: Some(data),
    }
  }

  pub fn ok_full(data: T, message: impl AsRef<str>) -> Self {
    Self {
      err: false,
      message: Some(message.as_ref().to_string()),
      data: Some(data),
    }
  }

  pub fn ok_with_data(data: T) -> Self {
    Self {
      err: false,
      message: None,
      data: Some(data),
    }
  }
}

impl Resp<String> {
  pub fn err_with_message(message: impl AsRef<str>) -> Self {
    Self {
      err: true,
      message: Some(message.as_ref().to_string()),
      data: None,
    }
  }

  pub fn ok_with_message(message: impl AsRef<str>) -> Self {
    Self {
      err: false,
      message: Some(message.as_ref().to_string()),
      data: None,
    }
  }

  pub fn ok() -> Self {
    Self {
      err: false,
      message: Some("success".to_string()),
      data: None,
    }
  }
}

impl<T: Serialize> Resp<T> {
  pub fn to_json(&self) -> serde_json::Result<String> {
    serde_json::to_string(self)
  }

  pub fn to_json_or(&self, def: &impl Serialize) -> String {
    let or_json = serde_json::to_string(def)
      .unwrap_or_else(|_| r#"{"err":true,"message":"Unknown"}"#.to_string());
    self.to_json().unwrap_or(or_json)
  }

  pub fn to_json_or_err(&self) -> String {
    self.to_json_or(&Resp::err_with_message("Unknown"))
  }

  pub fn to_json_or_ok(&self) -> String {
    self.to_json_or(&Resp::ok())
  }
}
