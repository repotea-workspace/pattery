create table t_demo
(
  id    varchar(50) not null,
  ctime timestamp default now(),
  primary key (id)
);

comment
on table t_demo is 'The demo table';
comment
on column t_demo.id is 'Primary key';
