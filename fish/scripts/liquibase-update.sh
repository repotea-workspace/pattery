#!/bin/sh
#

BIN_PATH=$(cd "$(dirname "$0")"; pwd -P)
WORK_PATH=${BIN_PATH}/../

LIQUIBASE_STORAGE_PATH=${WORK_PATH}/liquibase
LIQUIBASE_STORAGE_PATH_CHANGELOGS=${LIQUIBASE_STORAGE_PATH}/changelogs
LIQUIBASE_STORAGE_PATH_DATA=${LIQUIBASE_STORAGE_PATH}/data

LIQUIBASE=liquibase

DEF_LIQUIBASE_COMMAND_DRIVER='org.postgresql.Driver'
LIQUIBASE_COMMAND_DRIVER=${LIQUIBASE_COMMAND_DRIVER:-$DEF_LIQUIBASE_COMMAND_DRIVER}

# LIQUIBASE_COMMAND_URL=jdbc:postgresql://127.0.0.1:5432/pattery_stg
# LIQUIBASE_COMMAND_USERNAME=postgres
# LIQUIBASE_COMMAND_PASSWORD=passwd

cd ${WORK_PATH}/liquibase

#LIQUIBASE_COMMAND_CHANGELOG_FILE=${LIQUIBASE_COMMAND_CHANGELOG_FILE:-$LIQUIBASE_STORAGE_PATH}
LIQUIBASE_COMMAND_CHANGELOG_FILE='master.xml'

${LIQUIBASE} update \
  --url ${LIQUIBASE_COMMAND_URL} \
  --username ${LIQUIBASE_COMMAND_USERNAME} \
  --password ${LIQUIBASE_COMMAND_PASSWORD} \
  --changelog-file ${LIQUIBASE_COMMAND_CHANGELOG_FILE}

#liquibase --driver=com.mysql.jdbc.Driver \
#  --changeLogFile=changelog-20180330130430.xml \
#  --url="jdbc:mysql://localhost:3306/TestDB?useSSL=false" \
#  --classpath=mysql-connector-java-5.1.46.jar \
#  --username=matthung \
#  --password=12345 update


#$ liquibase update -h
#Deploy any changes in the changelog file that have not been deployed
#
#Usage: liquibase update [OPTIONS]
#
#      --change-exec-listener-class=PARAM
#                         Fully-qualified class which specifies a
#                           ChangeExecListener
#                         (liquibase.command.changeExecListenerClass OR
#                           liquibase.command.update.changeExecListenerClass)
#                         (LIQUIBASE_COMMAND_CHANGE_EXEC_LISTENER_CLASS OR
#                           LIQUIBASE_COMMAND_UPDATE_CHANGE_EXEC_LISTENER_CLASS)
#                         [deprecated: --changeExecListenerClass]
#
#      --change-exec-listener-properties-file=PARAM
#                         Path to a properties file for the
#                           ChangeExecListenerClass
#                         (liquibase.command.changeExecListenerPropertiesFile OR
#                           liquibase.command.update.
#                           changeExecListenerPropertiesFile)
#                         (LIQUIBASE_COMMAND_CHANGE_EXEC_LISTENER_PROPERTIES_FILE
#                            OR
#                           LIQUIBASE_COMMAND_UPDATE_CHANGE_EXEC_LISTENER_PROPERT
#                           IES_FILE)
#                         [deprecated: --changeExecListenerPropertiesFile]
#
#      --changelog-file=PARAM
#                         [REQUIRED] The root changelog
#                         (liquibase.command.changelogFile OR liquibase.command.
#                           update.changelogFile)
#                         (LIQUIBASE_COMMAND_CHANGELOG_FILE OR
#                           LIQUIBASE_COMMAND_UPDATE_CHANGELOG_FILE)
#                         [deprecated: --changelogFile]
#
#      --contexts=PARAM   Changeset contexts to match
#                         (liquibase.command.contexts OR liquibase.command.
#                           update.contexts)
#                         (LIQUIBASE_COMMAND_CONTEXTS OR
#                           LIQUIBASE_COMMAND_UPDATE_CONTEXTS)
#
#  -D=PARAM               Pass a name/value pair for substitution in the
#                           changelog(s)
#                         Pass as -D<property.name>=<property.value>
#                         [deprecated: set changelog properties in defaults file
#                           or environment variables]
#      --default-catalog-name=PARAM
#                         The default catalog name to use for the database
#                           connection
#                         (liquibase.command.defaultCatalogName OR liquibase.
#                           command.update.defaultCatalogName)
#                         (LIQUIBASE_COMMAND_DEFAULT_CATALOG_NAME OR
#                           LIQUIBASE_COMMAND_UPDATE_DEFAULT_CATALOG_NAME)
#                         [deprecated: --defaultCatalogName]
#
#      --default-schema-name=PARAM
#                         The default schema name to use for the database
#                           connection
#                         (liquibase.command.defaultSchemaName OR liquibase.
#                           command.update.defaultSchemaName)
#                         (LIQUIBASE_COMMAND_DEFAULT_SCHEMA_NAME OR
#                           LIQUIBASE_COMMAND_UPDATE_DEFAULT_SCHEMA_NAME)
#                         [deprecated: --defaultSchemaName]
#
#      --driver=PARAM     The JDBC driver class
#                         (liquibase.command.driver OR liquibase.command.update.
#                           driver)
#                         (LIQUIBASE_COMMAND_DRIVER OR
#                           LIQUIBASE_COMMAND_UPDATE_DRIVER)
#
#      --driver-properties-file=PARAM
#                         The JDBC driver properties file
#                         (liquibase.command.driverPropertiesFile OR liquibase.
#                           command.update.driverPropertiesFile)
#                         (LIQUIBASE_COMMAND_DRIVER_PROPERTIES_FILE OR
#                           LIQUIBASE_COMMAND_UPDATE_DRIVER_PROPERTIES_FILE)
#                         [deprecated: --driverPropertiesFile]
#
#  -h, --help             Show this help message and exit
#      --labels=PARAM     Changeset labels to match
#                         (liquibase.command.labels OR liquibase.command.update.
#                           labels)
#                         (LIQUIBASE_COMMAND_LABELS OR
#                           LIQUIBASE_COMMAND_UPDATE_LABELS)
#
#      --password=PARAM   Password to use to connect to the database
#                         (liquibase.command.password OR liquibase.command.
#                           update.password)
#                         (LIQUIBASE_COMMAND_PASSWORD OR
#                           LIQUIBASE_COMMAND_UPDATE_PASSWORD)
#
#      --url=PARAM        [REQUIRED] The JDBC database connection URL
#                         (liquibase.command.url OR liquibase.command.update.url)
#                         (LIQUIBASE_COMMAND_URL OR LIQUIBASE_COMMAND_UPDATE_URL)
#
#      --username=PARAM   Username to use to connect to the database
#                         (liquibase.command.username OR liquibase.command.
#                           update.username)
#                         (LIQUIBASE_COMMAND_USERNAME OR
#                           LIQUIBASE_COMMAND_UPDATE_USERNAME)
#
#
#Each argument contains the corresponding 'configuration key' in parentheses. As
#an alternative to passing values on the command line, these keys can be used as
#a basis for configuration settings in other locations.
#
#Available configuration locations, in order of priority:
#- Command line arguments (argument name in --help)
#- Java system properties (configuration key listed above)
#- Environment values (env variable listed above)
#- Defaults file (configuration key OR argument name)
#
#Full documentation is available at
#https://docs.liquibase.com
