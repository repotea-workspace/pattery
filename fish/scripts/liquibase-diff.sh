#!/bin/sh
#

BIN_PATH=$(cd "$(dirname "$0")"; pwd -P)
WORK_PATH=${BIN_PATH}/../

LIQUIBASE_STORAGE_PATH=${WORK_PATH}/liquibase
LIQUIBASE_STORAGE_PATH_CHANGELOGS=${LIQUIBASE_STORAGE_PATH}/changelogs
LIQUIBASE_STORAGE_PATH_DATA=${LIQUIBASE_STORAGE_PATH}/data

LIQUIBASE=liquibase

DEF_LIQUIBASE_COMMAND_DRIVER='org.postgresql.Driver'
LIQUIBASE_COMMAND_DRIVER=${LIQUIBASE_COMMAND_DRIVER:-$DEF_LIQUIBASE_COMMAND_DRIVER}

# tables,views,columns,indexs,foreignkeys,primarykeys,uniqueconstraints,data
DEF_LIQUIBASE_COMMAND_DIFF_TYPES='tables,columns,indexs,primarykeys,uniqueconstraints'
LIQUIBASE_COMMAND_DIFF_TYPES=${LIQUIBASE_COMMAND_DIFF_TYPES:-$DEF_LIQUIBASE_COMMAND_DIFF_TYPES}
LIQUIBASE_COMMAND_EXCLUDE_OBJECTS=${LIQUIBASE_COMMAND_EXCLUDE_OBJECTS:-flyway_schema_history}

# LIQUIBASE_COMMAND_REFERENCE_URL=jdbc:postgresql://127.0.0.1:5432/pattery_dev
# LIQUIBASE_COMMAND_REFERENCE_USERNAME=postgres
# LIQUIBASE_COMMAND_REFERENCE_PASSWORD=passwd
# LIQUIBASE_COMMAND_URL=jdbc:postgresql://127.0.0.1:5432/pattery_stg
# LIQUIBASE_COMMAND_USERNAME=postgres
# LIQUIBASE_COMMAND_PASSWORD=passwd

DIFF_FILE="${LIQUIBASE_STORAGE_PATH_CHANGELOGS}/$(date +%s)_changelog.xml"

$LIQUIBASE diffChangeLog \
  --driver ${LIQUIBASE_COMMAND_DRIVER} \
  --diff-types ${LIQUIBASE_COMMAND_DIFF_TYPES} \
  --changelog-file ${DIFF_FILE} \
  --url ${LIQUIBASE_COMMAND_URL} \
  --username ${LIQUIBASE_COMMAND_USERNAME} \
  --password ${LIQUIBASE_COMMAND_PASSWORD} \
  --reference-url ${LIQUIBASE_COMMAND_REFERENCE_URL} \
  --reference-username ${LIQUIBASE_COMMAND_REFERENCE_USERNAME} \
  --reference-password ${LIQUIBASE_COMMAND_REFERENCE_PASSWORD} \
  --excludeObjects ${LIQUIBASE_COMMAND_EXCLUDE_OBJECTS}

echo 'Data changelog write to '${DIFF_FILE}

#$ liquibase diff -h
#Compare two databases
#
#Usage: liquibase diff [OPTIONS]
#
#      --diff-types=PARAM   Types of objects to compare
#                           (liquibase.command.diffTypes OR liquibase.command.
#                             diff.diffTypes)
#                           (LIQUIBASE_COMMAND_DIFF_TYPES OR
#                             LIQUIBASE_COMMAND_DIFF_DIFF_TYPES)
#                           [deprecated: --diffTypes]
#
#      --driver=PARAM       The JDBC driver class
#                           (liquibase.command.driver OR liquibase.command.diff.
#                             driver)
#                           (LIQUIBASE_COMMAND_DRIVER OR
#                             LIQUIBASE_COMMAND_DIFF_DRIVER)
#
#      --driver-properties-file=PARAM
#                           The JDBC driver properties file
#                           (liquibase.command.driverPropertiesFile OR liquibase.
#                             command.diff.driverPropertiesFile)
#                           (LIQUIBASE_COMMAND_DRIVER_PROPERTIES_FILE OR
#                             LIQUIBASE_COMMAND_DIFF_DRIVER_PROPERTIES_FILE)
#                           [deprecated: --driverPropertiesFile]
#
#      --exclude-objects=PARAM
#                           Objects to exclude from diff
#                           (liquibase.command.excludeObjects OR liquibase.
#                             command.diff.excludeObjects)
#                           (LIQUIBASE_COMMAND_EXCLUDE_OBJECTS OR
#                             LIQUIBASE_COMMAND_DIFF_EXCLUDE_OBJECTS)
#                           [deprecated: --excludeObjects]
#
#      --format=PARAM       Option to create JSON output
#                           (liquibase.command.format OR liquibase.command.diff.
#                             format)
#                           (LIQUIBASE_COMMAND_FORMAT OR
#                             LIQUIBASE_COMMAND_DIFF_FORMAT)
#
#  -h, --help               Show this help message and exit
#      --include-objects=PARAM
#                           Objects to include in diff
#                           (liquibase.command.includeObjects OR liquibase.
#                             command.diff.includeObjects)
#                           (LIQUIBASE_COMMAND_INCLUDE_OBJECTS OR
#                             LIQUIBASE_COMMAND_DIFF_INCLUDE_OBJECTS)
#                           [deprecated: --includeObjects]
#
#      --password=PARAM     The target database password
#                           (liquibase.command.password OR liquibase.command.
#                             diff.password)
#                           (LIQUIBASE_COMMAND_PASSWORD OR
#                             LIQUIBASE_COMMAND_DIFF_PASSWORD)
#
#      --reference-password=PARAM
#                           The reference database password
#                           (liquibase.command.referencePassword OR liquibase.
#                             command.diff.referencePassword)
#                           (LIQUIBASE_COMMAND_REFERENCE_PASSWORD OR
#                             LIQUIBASE_COMMAND_DIFF_REFERENCE_PASSWORD)
#                           [deprecated: --referencePassword]
#
#      --reference-url=PARAM
#                           [REQUIRED] The JDBC reference database connection URL
#                           (liquibase.command.referenceUrl OR liquibase.command.
#                             diff.referenceUrl)
#                           (LIQUIBASE_COMMAND_REFERENCE_URL OR
#                             LIQUIBASE_COMMAND_DIFF_REFERENCE_URL)
#                           [deprecated: --referenceUrl]
#
#      --reference-username=PARAM
#                           The reference database username
#                           (liquibase.command.referenceUsername OR liquibase.
#                             command.diff.referenceUsername)
#                           (LIQUIBASE_COMMAND_REFERENCE_USERNAME OR
#                             LIQUIBASE_COMMAND_DIFF_REFERENCE_USERNAME)
#                           [deprecated: --referenceUsername]
#
#      --schemas=PARAM      Schemas to include in diff
#                           (liquibase.command.schemas OR liquibase.command.diff.
#                             schemas)
#                           (LIQUIBASE_COMMAND_SCHEMAS OR
#                             LIQUIBASE_COMMAND_DIFF_SCHEMAS)
#
#      --url=PARAM          [REQUIRED] The JDBC target database connection URL
#                           (liquibase.command.url OR liquibase.command.diff.url)
#                           (LIQUIBASE_COMMAND_URL OR LIQUIBASE_COMMAND_DIFF_URL)
#
#      --username=PARAM     The target database username
#                           (liquibase.command.username OR liquibase.command.
#                             diff.username)
#                           (LIQUIBASE_COMMAND_USERNAME OR
#                             LIQUIBASE_COMMAND_DIFF_USERNAME)
#
#
#Each argument contains the corresponding 'configuration key' in parentheses. As
#an alternative to passing values on the command line, these keys can be used as
#a basis for configuration settings in other locations.
#
#Available configuration locations, in order of priority:
#- Command line arguments (argument name in --help)
#- Java system properties (configuration key listed above)
#- Environment values (env variable listed above)
#- Defaults file (configuration key OR argument name)
#
#Full documentation is available at
#https://docs.liquibase.com

