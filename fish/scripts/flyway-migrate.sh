#!/bin/sh
#

BIN_PATH=$(cd "$(dirname "$0")"; pwd -P)
WORK_PATH=${BIN_PATH}/../

FLYWAY=flyway

# https://flywaydb.org/documentation/configuration/parameters/user
FLYWAY_USER=${FLYWAY_USER}
# https://flywaydb.org/documentation/configuration/parameters/password
FLYWAY_PASSWORD=${FLYWAY_PASSWORD}
# https://flywaydb.org/documentation/configuration/parameters/url
FLYWAY_URL=${FLYWAY_URL}
# https://flywaydb.org/documentation/configuration/parameters/locations
FLYWAY_LOCATIONS=${FLYWAY_LOCATIONS:-filesystem:${WORK_PATH}/records}
# https://flywaydb.org/documentation/configuration/parameters/baselineOnMigrate
FLYWAY_BASELINE_ON_MIGRATE=${FLYWAY_BASELINE_ON_MIGRATE}

#FLYWAY_USER=postgres
#FLYWAY_PASSWORD=passwd
#FLYWAY_URL=jdbc:postgresql://127.0.0.1:5432/pattery_stg
#FLYWAY_LOCATIONS=/path/to/migrate

${FLYWAY} \
  -user=${FLYWAY_USER} \
  -password=${FLYWAY_PASSWORD} \
  -url=${FLYWAY_URL} \
  -locations=${FLYWAY_LOCATIONS} \
  ${FLYWAY_BASELINE_ON_MIGRATE:+-baselineOnMigrate=true} migrate

