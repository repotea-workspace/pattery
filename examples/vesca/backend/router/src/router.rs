use actix_web::web;

use crate::route;

/// strawberry router
pub struct StrawberryRouter;

impl StrawberryRouter {
  pub fn router(cfg: &mut web::ServiceConfig) {
    Self::middleware_generic(cfg);
    Self::route_generic(cfg);
    Self::route_v1(cfg);
  }
}

/// generic router
impl StrawberryRouter {
  fn route_generic(cfg: &mut web::ServiceConfig) {
    cfg.route("/", web::get().to(route::generic::index));
  }

  fn middleware_generic(_cfg: &mut web::ServiceConfig) {}
}

/// v1 router
impl StrawberryRouter {
  fn route_v1(cfg: &mut web::ServiceConfig) {
    let scope = web::scope("/v1");
    let scope_route = scope.route("/hello", web::get().to(route::generic::index));
    cfg.service(scope_route);
  }
}
