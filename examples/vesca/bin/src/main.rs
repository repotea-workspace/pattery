use actix_web::{middleware, App, HttpServer};

use strawberry_common::envh;
use strawberry_common::types::RunningMode;
use strawberry_config::types::StrawberryConfig;
use strawberry_initializer::AppInitializer;
use strawberry_state::state::AppStateBuilder;
use vesca_router::router::StrawberryRouter;

/// Start a server and use a `Router` to dispatch requests
#[actix_web::main]
async fn main() -> color_eyre::Result<()> {
  let config = init_app().await?;
  if envh::running_mode() == RunningMode::Develop {
    tracing::debug!(target: "strawberry", "CONFIG -> \n{:#?}", config);
  }
  start_server(config).await?;
  Ok(())
}

async fn init_app() -> color_eyre::Result<StrawberryConfig> {
  Ok(AppInitializer.init()?)
}

async fn start_server(config: StrawberryConfig) -> color_eyre::Result<()> {
  let server_config = &config.server;
  let (host, port) = (server_config.host.clone(), server_config.port);
  tracing::info!(
    target: "strawberry",
    "The strawberry server listen {}:{}",
    &host,
    port,
  );
  let app_data = AppStateBuilder::new(config).app_state().await?;
  HttpServer::new(move || {
    App::new()
      .app_data(app_data.clone())
      .configure(StrawberryRouter::router)
      .wrap(middleware::Logger::default())
  })
  .bind((host, port))?
  .run()
  .await?;
  Ok(())
}
